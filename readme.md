# Standard Web Archetype

Java Standard Web Maven Archetype.

## Como criar um projeto

### "Instale" o archetype no seu repositório local:

```
git clone https://github.com/ubiplace/standard-web-archetype
cd standard-web-archetype
mvn install
```

Só precisa fazer isso uma única vez. Esse passo é necessário por que esse archetype não está disponível nos repositórios oficiais do Maven.

### Crie o seu projeto a partir do archetype:

```
mvn archetype:generate -DarchetypeGroupId=place.ubi -DarchetypeArtifactId=standard-web-archetype -DarchetypeVersion=0.0.1-SNAPSHOT -DgroupId=my.group -DartifactId=my-project -Dversion=0.0.1-SNAPSHOT -DinteractiveMode=false
```

## Como esse foi criado

Esse archetype começou a ser construído a partir do seguinte comando:

```
mvn archetype:generate -DarchetypeArtifactId=maven-archetype-archetype -DgroupId=place.ubi -DartifactId=standard-web-archetype -Dversion=0.0.1-SNAPSHOT -DinteractiveMode=false
```
